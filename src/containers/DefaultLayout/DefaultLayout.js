import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';

import {
  AppAside,
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
import navigation from '../../_nav';
// routes config
import routes from '../../routes';
import DefaultAside from './DefaultAside';
import DefaultFooter from './DefaultFooter';
import DefaultHeader from './DefaultHeader';

import { connect } from 'react-redux';

class DefaultLayout extends Component {
  constructor(props){
    super(props)
    this.state = {
    }
  }
    
  
  render() {
    console.log(this.props);
    return (
      <div className="app">
        <AppHeader fixed>
          <DefaultHeader 
            dip={this.props.user} 
            paciente={this.props.paciente}
            nombre={this.props.paciente.primer_apellido+' '+this.props.paciente.segundo_apellido+' '+this.props.paciente.nombres} 
            hc={this.props.hc} 
            historialclinico={this.props.historialclinico}
          />
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            <AppSidebarNav navConfig={navigation} {...this.props} />
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            <AppBreadcrumb appRoutes={routes}/>
            <Container fluid>
              <Switch>
                {routes.map((route, idx) => {
                    return route.component ? (<Route key={idx} path={route.path} exact={route.exact} name={route.name} render={props => (
                        <route.component {...props} />
                      )} />)
                      : (null);
                  },
                )}
                {/* <Redirect from="/" to="/dashboard" /> */}
                <Redirect from="/" to="/historiaclinica" />
              </Switch>
            </Container>
          </main>
          <AppAside fixed hidden>
            <DefaultAside />
          </AppAside>
        </div>
        <AppFooter>
          <DefaultFooter />
        </AppFooter>
      </div>
    );
  }
}

const mapStateToProps = state => ({
    user: state.user,
    paciente: state.paciente,
    hc: state.hc,
    historialclinico: state.historialclinico,
});

export default connect(mapStateToProps)(DefaultLayout);
