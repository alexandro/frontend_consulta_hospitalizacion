import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardTitle,
    CardText,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
    NavLink,
  } from 'reactstrap';
  import ColaInternacion from '../ColaInternacion';

class MenuInternacion extends Component {
    constructor(props){
        super(props)

        var today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        this.state = {
            internados: [],
            internado: [],
            anamnesis: [],
            primary: false,
            Nombre:'',
            Carrera:'',
            Facultad:'',
            Direccion:'',
            Telefono: 67000012,
            Fecha: date,
            Hora:'',
            Descripcion:'',
            Diagnostico:'',
            Indicaciones:'',
            
            date: date
        }
    }

    render() {        
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col xs="4" md="4">  

                    <Card body inverse style={{ backgroundColor: '#248b96', borderColor: '#248b96' }}>
                        <CardTitle><center><h1>Anamnesis</h1></center></CardTitle>
                        <CardText><center>PROMES - SSU</center></CardText>
                        <NavLink href={'#/colainternacion/menuinternacion/anamnesis/' + this.props.match.params.ci} >
                            <Button size="lg" block outline color="secondary">Ver</Button>
                        </NavLink>
                    </Card>
                    </Col>

                    <Col xs="4" md="4">
                    <Card body inverse style={{ backgroundColor: '#333', borderColor: '#333' }}>
                        <CardTitle><center><h1>Nota de Internación</h1></center></CardTitle>
                        <CardText><center>PROMES - SSU</center></CardText>
                        <NavLink href={'#/colainternacion/menuinternacion/NotaInternacionE/' + this.props.match.params.ci} >
                            <Button size="lg" block outline color="secondary">Ver</Button>
                        </NavLink>
                    </Card>
                    </Col>

                    <Col xs="4" md="4">
                    <Card body inverse style={{ backgroundColor: '#2d3d91', borderColor: '#2d3d91' }}>
                        <CardTitle><center><h1>Hoja de Referencia</h1></center></CardTitle>
                        <CardText><center>PROMES - SSU</center></CardText>
                        <NavLink href={'#/colainternacion/menuinternacion/hojareferencia/' + this.props.match.params.ci} >
                            <Button size="lg" block outline color="secondary">Ver</Button>
                        </NavLink>
                    </Card>
                    </Col>
                </Row>
        </div>
        );
    }
}
export default MenuInternacion;