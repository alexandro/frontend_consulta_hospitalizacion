import React, { Component } from 'react'
import {
    Col,
    Input,
    FormGroup,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
} from 'reactstrap';

class HabitoFrecuencia extends Component {
    constructor(props){
        super(props)

        this.state = {
            dropdownOpen: false,
        }
    }
    render() {
        return (
            <div>
                <FormGroup row>
                    <Col md="2">
                        Frecuencia:
                    </Col>
                    <Col md="2" >
                        <Input 
                            size="lg"
                            type="number"
                            onChange={e => this.setState({acantidad: e.target.value})}
                        />
                    </Col>
                    <Col md="1">veces</Col>
                    <Col>
                        <Dropdown size="lg" isOpen={this.state.dropdownHabitos[0]} toggle={() => {this.toggleHabitos(0)}}>
                            <DropdownToggle caret>
                                {this.state.afrecuencia}
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem header>Frecuencia</DropdownItem>
                                <DropdownItem value="al día" onClick={e => this.setState({afrecuencia: e.target.value})}>al día</DropdownItem>
                                <DropdownItem value="a la semana" onClick={e => this.setState({afrecuencia: e.target.value})}>a la semana</DropdownItem>
                                <DropdownItem value="al mes" onClick={e => this.setState({afrecuencia: e.target.value})}>al mes</DropdownItem>
                                <DropdownItem value="al año" onClick={e => this.setState({afrecuencia: e.target.value})}>al año</DropdownItem>
                            </DropdownMenu>
                        </Dropdown>
                    </Col>
                </FormGroup>
            </div>
        )
    }
}
export default HabitoFrecuencia;