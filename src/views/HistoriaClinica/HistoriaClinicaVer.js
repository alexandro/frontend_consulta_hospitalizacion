import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
    NavLink,
  } from 'reactstrap';
import Encabezado from '../Encabezado/Encabezado';
import html2canvas from 'html2canvas'
import jsPDF from 'jspdf';

import { connect } from 'react-redux';

class HistoriaClinicaVer extends Component {
    constructor(state){
        super(state)

        this.state = {
        }
    }

    crearPdf(){
        const input = document.getElementById('hc-imprimir');
        html2canvas(input)
            .then((canvas) => {
                const imgData = canvas.toDataURL('image/png');
                const pdf = new jsPDF();
                pdf.addImage(imgData, 'JPEG', 0, 0);
                pdf.save("hc-prueba.pdf");
            });
    }

   render() {
        return (
            <div className="animated fadeIn" class="historiaclinica" id="hc-imprimir">
                <Row>
                <Col md="12">
                <Card>
                    <CardBody>
                        <Encabezado />
                        <div class="thistoria"><h2><b>HISTORIA CLÍNICA</b></h2></div>
                        <div class="fhistoria"><h5><span>Fecha de consulta: {this.props.historialclinico.fecha_primeraconsulta} </span></h5></div>
                        <div class="fhistoria"><h5><span>Hora: {this.props.historialclinico.hora_primeraconsulta} </span></h5></div>
                        <div class="prueba-hr">
                            <h4 class="seccion">Datos Personales</h4>
                            <hr class="hr-dinamico"/>
                        </div>
                        <Table responsive borderless size="sm">
                            <tbody class>
                            <tr>
                                <td><b>Nombre: </b>{this.props.paciente.primer_apellido + ' ' + this.props.paciente.segundo_apellido + ' ' + this.props.paciente.nombres}</td>
                                <td><b>Matrícula: </b>{this.props.paciente.matricula}</td>
                            </tr>
                            <tr>
                                <td><b>Sexo: </b>{this.props.paciente.id_sexo}</td>
                                <td><b>Edad: </b>{this.props.paciente.fec_nacimiento}</td>
                            </tr>
                            <tr>
                                <td><b>Procedencia: </b>{this.props.paciente.sede}</td>
                                <td><b>Residencia: </b>{this.props.paciente.sede}</td>
                            </tr>
                            <tr>
                                <td><b>Estado civil: </b>{this.props.paciente.estado_civil}</td>
                                <td><b>Domicilio: </b>{this.props.paciente.direccion}</td>
                            </tr>
                            </tbody>

                            {/* FALTA AÑADIR TELEFONOS */}
                        </Table> 
                        <div class="prueba-hr">
                            <h4 class="seccion">Recolección de datos</h4>
                            <hr class="hr-dinamico"/>
                        </div>
                        <FormGroup row>
                            <Col md="12">
                                <p>
                                    <b>Motivo Consulta: </b>{this.props.historialclinico.motivo_consulta}<br/>
                                    <b>Historia de la Enfermedad Actual: </b>{this.props.historialclinico.historia_enfermedad_actual}<br/>
                                </p>
                            </Col>
                        </FormGroup>
                        <h2 class="tseccion">ANAMNESIS</h2>
                    
                        <div class="prueba-hr">
                            <h4 class="seccion">Antecedentes Personales no patológicos</h4>
                            <hr class="hr-dinamico"/>
                        </div>
                        <FormGroup>
                            <p>
                                <b>Condiciones de vida: </b>
                            </p>
                            <ul>
                                <li><b>Vivienda: </b>{this.props.antecedentespnp.vivienda}</li>
                                <li><b>Servicio higienico intradomiciliario: </b>{this.props.antecedentespnp.servicio_higienico_intradomiciliario}</li>
                                <li><b>Agua intradomiciliara: </b>{this.props.antecedentespnp.agua_intradomiciliaria}</li>
                            </ul>
                            <p>
                                <b>Alimentación: </b> {this.props.antecedentespnp.alimentacion}<br/>
                                <b>Habito Alcohólico: </b> {this.props.antecedentespnp.habito_alcoholico}<br/>
                                <b>Habito Tabáquico: </b> {this.props.antecedentespnp.habito_tabaquico}<br/>
                                <b>Somnia: </b> {this.props.antecedentespnp.somnia}<br/>
                                <b>Diuresis: </b> {this.props.antecedentespnp.diuresis}<br/>
                                <b>Hábito Intestinal: </b> {this.props.antecedentespnp.habito_intestinal}<br/>
                            </p>
                        </FormGroup>

                        <div class="prueba-hr">
                            <h4 class="seccion">Antecedentes personales patológicos</h4>
                            <hr class="hr-dinamico"/>
                        </div>
                        
                        <FormGroup row>
                            <Col md="12">
                                <Label><b>COMBE: </b>{this.props.antecedentespp.combe}</Label>
                            </Col>
                        </FormGroup>

                        <FormGroup row style={{textAlign: "center"}}>
                        <Col md="6">
                                <h4><b>CLÍNICOS</b></h4>
                            {this.props.antecedentesc.length !== 0 ?
                            <Table bordered responsive>
                                    <thead>
                                        <th>Antecedente</th>
                                    </thead>
                                    <tbody>
                                        {this.props.antecedentesc.map((item) => (
                                            <tr key={item.id}>
                                                <td>{item.antecedentec}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                            </Table>
                            :
                            <Table bordered responsive>
                                    <tbody>
                                        <tr>
                                            <td>No refiere</td>
                                        </tr>
                                    </tbody>
                            </Table>
                            }
                        </Col>
                        
                        <Col md="6">
                                <h4><b>QUIRÚRGICOS</b></h4>
                            {this.props.antecedentesq.length !== 0 ?
                            <Table bordered responsive>
                                    <thead>
                                        <tr>
                                            <th>Tratamiento Quirúrgico</th>
                                            <th>Año</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.props.antecedentesq.map((item) => (
                                            <tr key={item.id}>
                                                <td>{item.tratamiento_quirurgico}</td>
                                                <td>{item.ano}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                            </Table>
                            :
                            <Table bordered responsive>
                                    <tbody>
                                        <tr>
                                            <td>No refiere</td>
                                        </tr>
                                    </tbody>
                            </Table>
                            }
                        </Col>
                        </FormGroup>
                        
                        {this.props.alergias.length !== 0 ?
                            <div>
                            <p>
                                <b>Alergias: </b>
                            </p>
                            <ul>
                                {this.props.alergias.map((item) => (
                                    <li key={item.id}>{item.alergia}</li>
                                ))}
                            </ul>
                            </div>
                            :
                            <p>
                                <b>Alergias: </b>No refiere
                            </p>
                        }
                        {this.props.transfusiones.length !== 0 ?
                        <div>
                        <p>    
                            <b>Transfusiones: </b>  
                        </p>
                        <ul>
                            {this.props.transfusiones.map((item) => (
                                <li key={item.id}>{item.ano}, descripcion: {item.descripcion}</li>
                            ))}
                        </ul>
                        </div>
                        :
                        <p>    
                            <b>Transfusiones: </b>No refiere
                        </p>
                        }

                        <div class="prueba-hr">
                            <h4 class="seccion">Antecedentes familiares</h4>
                            <hr class="hr-dinamico"/>
                        </div>
                        {this.props.antecedentesfam.length !== 0 ?
                        <ul>
                            {this.props.antecedentesfam.map((item) => (
                                <li key={item.id}>{item.familiar} con {item.antecedentef}</li>
                            ))}
                        </ul>
                        :
                        <p>Sin antecedentes familiares</p>
                        }

                        {this.props.paciente.id_sexo=='F' ?
                            <div class="prueba-hr">
                                <h4 class="seccion">Antecedentes gineco obstétricos</h4>
                                <hr class="hr-dinamico"/>
                            </div>
                            :
                            <div class="prueba-hr" style={{opacity:'0.4'}}>
                                <h4 class="seccion">Antecedentes gineco obstétricos</h4>
                                <hr class="hr-dinamico"/>
                                <p>Deshabilitado</p>
                            </div>
                        }
                        {this.props.paciente.id_sexo == 'F' &&
                            <div>
                                <Table bordered row style={{textAlign: 'center'}}>
                                    <thead>
                                        <th>Gesta</th>
                                        <th>Menarca</th>
                                        <th>Para</th>
                                        <th>FUM</th>
                                        <th>Abortos</th>
                                        <th>Catamenio</th>
                                        <th>I.R.S.</th>
                                    </thead>
                                    <tbody>
                                        <td>{this.props.antecedentesgo.gesta}</td>
                                        <td>{this.props.antecedentesgo.menarca}</td>
                                        <td>{this.props.antecedentesgo.para}</td>
                                        <td>{this.props.antecedentesgo.fum}</td>
                                        <td>{this.props.antecedentesgo.abortos}</td>
                                        <td>{this.props.antecedentesgo.catamenio}</td>
                                        <td>{this.props.antecedentesgo.irs}</td>
                                    </tbody>
                                </Table>
                            </div>
                        }
                        
                        {this.props.planificacionf == 'Si' &&
                        <div class="prueba-hr">
                            <h4 class="seccion">Planificación familiar</h4>
                            <hr class="hr-dinamico"/>
                        </div>
                        }
                        {this.props.planificacionf == 'Si' &&
                            <Table bordered responsive row>
                                <thead>
                                    <th>Método</th>
                                    <th>Continua</th>
                                    <th>Causa de abandono</th>
                                </thead>
                                <tbody>
                                    <td>{this.props.planificacionf.metodo}</td>
                                    <td>{this.props.planificacionf.continua}</td>
                                    <td>{this.props.planificacionf.causa_abandono}</td>
                                </tbody>
                            </Table>
                        }
                        <div class="prueba-hr">
                            <h4 class="seccion">Signos vitales</h4>
                            <hr class="hr-dinamico"/>
                        </div>
                        <Table bordered responsive responsive>
                            <center>
                                <thead>
                                    <th>Talla</th>
                                    <th>Peso</th>
                                    <th>Temperatura</th>
                                    <th>Pulso</th>
                                    <th>Presion Arterial</th>
                                    
                                    {/* <th>Frecuencia Cardiaca</th> */}
                                    {/* <th>Frecuencia Respiratoria</th> */}
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{this.props.signosv.talla}</td>
                                        <td>{this.props.signosv.peso}</td>
                                        <td>{this.props.signosv.temperatura}</td>
                                        <td>{this.props.signosv.pulso}</td>
                                        <td>{this.props.signosv.pa}</td>
                                        {/* FALTA TEMPERATURA AXILAR */}
                                    </tr>
                                </tbody>
                            </center>
                        </Table>
                        <div class="prueba-hr">
                            <h4 class="seccion">Examen físico</h4>
                            <hr class="hr-dinamico"/>
                        </div>
                        <div style={{textAlign: 'justify'}}>
                        <p>
                            <h5><b>Estado General</b></h5>
                            {this.props.examenf.estado_general}
                        </p>
                        <p>
                            <h5><b>Piel y faneras</b></h5>
                            {this.props.examenf.piel_faneras}
                        </p>
                        <p>
                            <h5><b>Cabeza y cuello</b></h5>
                            {this.props.examenf.cara_cuello}
                        </p>
                        <p>
                            <h5><b>Visual y Auditivo</b></h5>
                            {this.props.examenf.visual_auditivo}
                        </p>
                        <p>
                            <h5><b>Tiroides</b></h5>
                            {this.props.examenf.tiroides}
                        </p>
                        <p>
                            <h5><b>Cardiopulmonar</b></h5>
                            {this.props.examenf.cardiopulmonar}
                        </p>
                        <p>
                            <h5><b>Abdomen</b></h5>
                            {this.props.examenf.abdomen}
                        </p>
                        <p>
                            <h5><b>Genitales</b></h5>
                            {this.props.examenf.genitales}
                        </p>
                        <p>
                            <h5><b>Extremidades</b></h5>
                            {this.props.examenf.extremidades}
                        </p>
                        <p>
                            <h5><b>Neurologico</b></h5>
                            {this.props.examenf.neurologico}
                        </p>
                        </div>

                        <hr class="diagnostico"/>

                        {this.props.historialclinico.datos_complementarios.length === 0 || !this.props.historialclinico.datos_complementarios.trim() ?
                            <p>
                                <b>Datos complementarios: </b> No refiere
                            </p>
                            :
                            <p>
                                <b>Datos complementarios: </b> {this.props.historialclinico.datos_complementarios}
                            </p>
                        }
                        
                        <div class="prueba-hr">
                            <h4 class="seccion">Impresión Diagnóstica</h4>
                            <hr class="hr-dinamico"/>
                        </div>
                        <div class="diagnosticop-vista">
                            <span>{this.props.historialclinico.diagnostico_presuntivo}</span>
                        </div>
                      
                    </CardBody>
                    {/* <Button onClick={this.crearPdf}>
                        Generar PDF
                    </Button> */}
                </Card>
                
                </Col>
            </Row>

            {/* {this.state.show && <NotaInternacion afiliadox={this.state.afiliadox} id={this.state.id}/>} */}
        </div>
        );
    }
}

const mapStateToProps = state => ({
    paciente: state.paciente,
    historialclinico: state.historialclinico,
    anamnesis: state.anamnesis,
    examenf: state.examenf,
    laboratorios: state.laboratorios,
    antecedentespnp: state.antecedentespnp,
    antecedentespp: state.antecedentespp,
    antecedentesfam: state.antecedentesfam,
    planificacionf: state.planificacionf,
    antecedentesgo: state.antecedentesgo,
    transfusiones: state.transfusiones,
    alergias: state.alergias,
    antecedentesq: state.antecedentesq,
    antecedentesc: state.antecedentesc,
    signosv: state.signosv,
});

export default connect(mapStateToProps)(HistoriaClinicaVer);