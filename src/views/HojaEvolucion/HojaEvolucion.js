import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Popover,
    PopoverHeader,
    PopoverBody,
    Row,
    Table,
  } from 'reactstrap';
  import Widget04 from '../Widgets/Widget04';
  import Encabezado from '../Encabezado/Encabezado';
  import EncabezadoDatos from '../EncabezadoDatos/EncabezadoDatos';

  import { connect } from 'react-redux';

  import ReactQuill from 'react-quill';
  import 'react-quill/dist/quill.snow.css';
  import renderHTML from 'react-render-html';

class HojaEvolucion extends Component {
    constructor(props){
        super(props)

        this.state = {
            subjetivo: '',
            objetivo: '',
            apreciacion: '',
            paccion:'',
            collapse: false,
            popover_signosvitales: false,
        }
        this.handleChangeSubjetivo = this.handleChangeSubjetivo.bind(this);
        this.handleChangeObjetivo = this.handleChangeObjetivo.bind(this);
        this.handleChangeApreciacion = this.handleChangeApreciacion.bind(this);
        this.handleChangePAccion = this.handleChangePAccion.bind(this);

        this.toggle_signosvitales = this.toggle_signosvitales.bind(this);
    }

    handleChangeSubjetivo(value){
        this.setState({subjetivo: value})
    }
    handleChangeObjetivo(value){
        this.setState({objetivo: value})
    }
    handleChangeApreciacion(value){
        this.setState({apreciacion: value})
    }
    handleChangePAccion(value){
        this.setState({paccion: value})
    }

    toggle_signosvitales() {
        this.setState({
            popover_signosvitales: !this.state.popover_signosvitales
        });
    }

   render() {
        return (
            <div className="animated fadeIn" class="historiaclinica" >
            <Row>

              <Col xs="12" sm="12" md="12">
                <Card>
                  <CardBody>
                  <Encabezado/>
                  <EncabezadoDatos titulo="HOJA DE EVOLUCION"/>
                    <FormGroup row>
                        <Button id="signos1" color="warning" onClick={this.toggle_signosvitales} style={{marginTop: '30px', marginLeft: 'auto', marginRight: 'auto', fontSize: '20px'}}>
                            Añadir Signos Vitales
                        </Button>
                    </FormGroup>

                    <Popover placement="bottom" isOpen={this.state.popover_signosvitales} target="signos1" toggle={this.toggle_signosvitales}>
                        <div style={{backgroundColor: '#ffc107', padding: '4px'}}>
                            <div style={{backgroundColor: '#fff', padding: '10px'}}>
                            <FormGroup style={{padding: '4px 0 2px', margin: '0', color:'#000'}} row>
                                <Col md="6">
                                    <span class="signosvitales-popover">Presión arterial</span>
                                </Col>
                                <Col md="6">
                                    <Input
                                        type="number"
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup style={{padding: '2px 0', margin: '0', color:'#000'}} row>
                                <Col md="6">
                                    <span class="signosvitales-popover">Peso</span>
                                </Col>
                                <Col md="6">
                                    <Input
                                        type="number"
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup style={{padding: '2px 0', margin: '0', color:'#000'}} row>
                                <Col md="6">
                                    <span class="signosvitales-popover">Pulso</span>
                                </Col>
                                <Col md="6">
                                    <Input
                                        type="number"
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup style={{padding: '2px 0', margin: '0', color:'#000'}} row>
                                <Col md="6">
                                    <span class="signosvitales-popover">Talla</span>
                                </Col>
                                <Col md="6">
                                    <Input
                                        type="number"
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup style={{padding: '2px 0 4px', margin: '0', color:'#000'}} row>
                                <Col md="6">
                                    <span class="signosvitales-popover">Temperatura</span>
                                </Col>
                                <Col md="6">
                                    <Input
                                        type="number"
                                    />
                                </Col>
                            </FormGroup>
                            </div>
                        </div>
                    </Popover>

                    <FormGroup id="soap" style={{backgroundColor: '#447E98', padding: '20px 0', margin: '0', color:'#fff'}} row>
                        <Col md="1">
                            <Label>Subjetivo</Label>
                        </Col>
                        <Col md="11">
                            <ReactQuill 
                                modules={HojaEvolucion.modules}
                                formats={HojaEvolucion.formats}
                                value={this.state.subjetivo}
                                onChange={this.handleChangeSubjetivo}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup class="soap" style={{backgroundColor: '#698d9e', padding: '20px 0', margin: '0', color:'#fff'}} row>
                        <Col md="1">
                            <Label>Objetivo</Label>
                        </Col>
                        <Col md="11">
                            <ReactQuill 
                                modules={HojaEvolucion.modules}
                                formats={HojaEvolucion.formats}
                                value={this.state.objetivo}
                                onChange={this.handleChangeObjetivo}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup class="soap" style={{backgroundColor: '#135472', padding: '20px 0', margin: '0', color:'#fff'}} row>
                        <Col md="1">
                            <Label>Apreciacion</Label>
                        </Col>
                        <Col md="11">
                            <ReactQuill 
                                modules={HojaEvolucion.modules}
                                formats={HojaEvolucion.formats}
                                value={this.state.apreciacion}
                                onChange={this.handleChangeApreciacion}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup class="soap" style={{backgroundColor: '#447E98', padding: '20px 0', margin: '0', color:'#fff'}} row>
                        <Col md="1">
                            <Label>Plan de acción</Label>
                        </Col>
                        <Col >
                            <ReactQuill 
                                modules={HojaEvolucion.modules}
                                formats={HojaEvolucion.formats}
                                value={this.state.paccion}
                                onChange={this.handleChangePAccion}
                            />
                        </Col>
                    </FormGroup>
                  </CardBody>

                    
                    {/* <h3>Nombre, Medico Tratante</h3>


                <Button onClick={e => this.setState({collapse: !this.state.collapse})}>Soy un boton</Button>
                <Collapse isOpen={this.state.collapse}>
                    <Card>
                        <CardBody>
                            Hola mundo          
                       </CardBody>
                    </Card>
                </Collapse> */}
                </Card>
              </Col>
            </Row>
          </div>
        );
    }
}

HojaEvolucion.modules = {
    toolbar: [
        [{header: '1'}, {header: '2'}, {'font': []}],
        [{size: []}],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [{'color': []}, {'background': []}, {'align': []}],
        [{'list': 'ordered'}, {'list': 'bullet'}],
        ['link', 'image'],
        ['clean'],
    ]
};

HojaEvolucion.formats = [
    'header', 'font', 'size',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'color', 'background', 'align',
    'list', 'bullet',
    'link', 'image', 'video', 'code-block'
]

const mapStateToProps = state => ({
    paciente: state.paciente,
    historialclinico: state.historialclinico
})

export default connect(mapStateToProps)(HojaEvolucion);