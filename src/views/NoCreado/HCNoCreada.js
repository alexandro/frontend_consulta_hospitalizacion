import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardTitle,
    CardText,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
    NavLink,
  } from 'reactstrap';

class HCNoCreada extends Component {
    constructor(props){
        super(props);
        this.handleHistoriaSw = this.handleHistoriaSw.bind(this);
    }

    handleHistoriaSw(e){
        this.props.onHistoriaSw(e.target.value);
    }

    render() {        
        return (
            <div className="animated fadeIn">
                <Card>
                    <div class="contenedor">
                        <div class="hc-nocreada">
                            <img src="./assets/img/maquinaDeEscribir.png" class="responsivog"/>
                            <h2>El paciente aún no tiene una Historia Clínica</h2>
                        </div>
                        <div class="botonhc2">
                            <NavLink onClick={this.handleHistoriaSw}><span>Redactar</span></NavLink>
                        </div>
                    </div>
                </Card>
            </div>
        );
    }
}
export default HCNoCreada;