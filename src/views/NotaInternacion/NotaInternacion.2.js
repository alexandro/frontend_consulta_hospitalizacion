import React, { Component } from 'react';

class NotaInternacion extends Component {
  state = {
    todos: []
  };

  async componentDidMount() {
    try {
      const res = await fetch('http://127.0.0.1:8000/internados/');
      const todos = await res.json();
      this.setState({
        todos
      });
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    return (
      <div>
        {this.state.todos.map(item => (
          <div key={item.id}>
            <h1>{item.nombre}</h1>
            <span>{item.direccion}</span>
          </div>
        ))}
      </div>
    );
  }
}

export default NotaInternacion;