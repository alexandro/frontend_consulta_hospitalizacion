import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
  } from 'reactstrap';

class NotaInternacion extends Component {
    constructor(props){
        super(props)

        this.state = {
            internados: [],
            primary: false,
            Nombre:'',
            Direccion:'',
            Telefono:'',
            Fecha:'',
            Hora:'',
            Descripcion:'',
            Diagnostico:'',
            Tratamiento:'',
        }

        this.handleOnSubmit = this.handleOnSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.togglePrimary = this.togglePrimary.bind(this)
    }

    async componentDidMount() {
        try{
            const respuesta = await fetch('http://127.0.0.1:8000/internados/');
            const internados = await respuesta.json();
            console.log(internados)
            this.setState({
                internados
            });
        } catch(e){
            console.log(e);
        }
    }

    handleChange(e){
        let {value, name, id} = e.target
        console.log(value, name, id)

        let internados_copia = this.state.internados

        if(name === "nombre"){
            internados_copia[id].nombre = value
            console.log(value)
        }else if(name === "direccion"){
            internados_copia[id].direccion = value
        }else if(name === "telefono"){
            internados_copia[id].telefono = value
        }else if(name === "fecha"){
            internados_copia[id].fecha = value
        }else if(name === "hora"){
            internados_copia[id].hora = value
        }else if(name === "descripcion"){
            internados_copia[id].descripcion = value
        }else if(name === "direccion"){
            internados_copia[id].direccion = value
        }else if(name === "indicaciones"){
            internados_copia[id].indicaciones = value
        }

        // if(name === "")
        this.setState({
            internados : internados_copia
        });

        console.log(internados_copia[id].nombre+"=>"+value)
        console.log(this.state.internados)
    }

    handleOnSubmit(e){
        e.preventDefault()

        this.setState({
            success: !this.state.success,
        });

        let url= ''
        let data = {}

            url = 'http://127.0.0.1:8000/internados/'
            data = {
                // nombre: nombre,
                // direccion: direccion,
                // telefono: telefono,
                // fecha: fecha,
                // hora: hora,
                // descripcion: descripcion,
                // diagnostico: diagnostico,
                // indicaciones: indicaciones
            }
            // console.log(data)

            try{
                fetch(url, {
                    method: 'POST',
                    body: JSON.stringify(this.state),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => console.log('Success:', response));
            }catch(e){
                console.log(e);
                alert(e);
            }
        

        this.setState({
            primary: !this.state.primary,
        });
    }

    togglePrimary() {
        this.setState({
            primary: !this.state.primary,
        });
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                <Col xs="12" md="12">
                <Card>
                    <CardHeader>
                    <strong>Nota de Internacion</strong> PROMES
                    </CardHeader>
                    <CardBody>
                    <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                        
                        <FormGroup row>
                        <Col md="3">
                            <Label>Static</Label>
                        </Col>
                        <Col xs="12" md="9">
                            {this.state.internados.map(item => (
                               <div key={item.id}>
                                    <h1>{item.nombre}</h1>
                               </div> 
                            ))}
                            {/* <p className="form-control-static">Username</p> */}
                        </Col>
                        </FormGroup>
                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input">Nombre</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="text" 
                                id="nombre" 
                                name="nombre" 
                                required
                                value={this.state.Nombre}
                                placeholder="Ej. Andres Mendoza Rojas" 
                                onChange={e => this.setState({ Nombre: e.target.value })}
                            />
                            <FormText color="muted">Coloca tu nombre completo</FormText>
                        </Col>
                        </FormGroup> 

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input">Dirección</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="text" 
                                id="direccion" 
                                name="direccion" 
                                placeholder="Ej. Zona Villa Copacabana" 
                                required
                                value={this.state.Direccion}
                                onChange={e => this.setState({ Direccion: e.target.value })}
                            />
                            <FormText color="muted">Coloca el lugar de tu residencia actual</FormText>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input">Número telefónico</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="number" 
                                id="telefono" 
                                name="telefono" 
                                placeholder="Ej. 700 00 000" 
                                
                                value={this.state.Telefono}
                                // OnChange={e => this.setState({ Telefono: e.target.value })}
                            />
                            <FormText color="muted">Coloca el lugar de tu residencia actual</FormText>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="date-input">Fecha de Atención</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="date" 
                                id="fecha" 
                                name="fecha" 
                                placeholder="date" 
                                required
                                value={this.state.Fecha}
                                OnChange={e => this.setState({ Fecha: e.target.value })}
                            />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="date-input">Hora de Atención</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="time" 
                                id="hora" 
                                name="hora"
                                required
                                value={this.state.Hora}
                                OnChange={e => this.setState({ Hora: e.target.value })}
                            />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input">Descripcion</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="text" 
                                name="descripcion" 
                                id="descripcion" 
                                // rows="9"
                                placeholder="Decripción del médico" 
                                // required
                                value={this.state.Descripcion}
                                OnChange={e => this.setState({ Descripcion: e.target.value })}
                            />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input">Diagnóstico</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="textarea" 
                                name="diagnostico" 
                                id="diagnostico" 
                                rows="9"
                                placeholder="Diagnóstico del médico"
                                required
                                value={this.state.Diagnostico} 
                                OnChange={e => this.setState({ Diagnostico: e.target.value })}
                            />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input">Tratamiento</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="textarea" 
                                name="tratamiento" 
                                id="tratamiento" 
                                rows="9"
                                placeholder="Tratamiento del médico" 
                                required
                                value={this.state.Tratamiento}
                                OnChange={e => this.setState({ Tratamiento: e.target.value })}
                            />
                        </Col>
                        </FormGroup>
                        
                        
                    </Form>
                    </CardBody>
                    <CardFooter>
                    <Button onClick={this.handleOnSubmit} type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enviar</Button>
                    <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Limpiar</Button>
                    </CardFooter>
                </Card>
                </Col>
            </Row>
        </div>
        );
    }
}
export default NotaInternacion;