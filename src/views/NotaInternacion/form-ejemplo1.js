import React from 'react';
import ReactDOM from 'react-dom';

class NotaInternacion extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      qwe: 'Please write an essay about your favorite DOM element.'
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({qwe: event.target.value});
  }

  handleSubmit(event) {
    alert('An essay was submitted: ' + this.state.qwe);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Essay:
          <textarea value={this.state.qwe} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}
export default NotaInternacion;