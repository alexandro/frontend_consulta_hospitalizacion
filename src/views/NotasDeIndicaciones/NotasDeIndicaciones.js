import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
  } from 'reactstrap';
  import Widget04 from '../Widgets/Widget04';
  import Encabezado from '../Encabezado/Encabezado';
  import EncabezadoDatos from '../EncabezadoDatos/EncabezadoDatos';

  import { connect } from 'react-redux';

class NotasDeIndicaciones extends Component {
    constructor(props){
        super(props)

        var hoy = new Date(),
            f = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();

        this.state = {
            fecha: f,
            hora: new Date(),
            collapse: false,
            indicaciones: [{indicacion: ''}],
        }
    }

    adicionaIndicacion = () => {
        this.setState({
            indicaciones: this.state.indicaciones.concat([{indicacion: ''}])
        })
    }
    adicionaIndicacionSetState = (idx) => (evt) => {
        const newIndicacion = this.state.indicaciones.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {indicacion: evt.target.value}
        })
        this.setState({indicaciones: newIndicacion})
    }
    eliminaIndicacion = (idx) => () => {
        this.setState({
            indicaciones: this.state.indicaciones.filter((s, sidx) => idx !== sidx)
        })
    }


   render() {
        return (
          <div className="animated fadeIn" class="historiaclinica">
            <Row>
            <Col xs="12" sm="12" md="12">
                <Card>
                    <CardBody>
                    <Encabezado/>
                    <EncabezadoDatos titulo="NOTAS DE INDICACIONES"/>

                    <div class="prueba-hr">
                        <h4 class="seccion">Descripción</h4>
                        <hr class="hr-dinamico"/>
                    </div>
                    {this.state.indicaciones.map((item, idx) => (
                        <FormGroup row key={idx}>
                            <Col md="12">
                                <InputGroup size="lg">
                                    <Input 
                                        placeholder="Nueva indicación..."
                                        size="lg"
                                        type="textarea"
                                        rows="10"
                                        value={item.indicacion}
                                        onChange={this.adicionaIndicacionSetState(idx)}
                                    />
                                    <InputGroupAddon addonType="append">
                                        <Button color="success" onClick={this.adicionaIndicacion}>+</Button>
                                    </InputGroupAddon>
                                    {idx > 0 &&
                                        <InputGroupAddon addonType="append">
                                            <Button color="danger" onClick={this.eliminaIndicacion(idx)}>-</Button>
                                        </InputGroupAddon>
                                    }
                                </InputGroup>
                            </Col>
                        </FormGroup>
                    ))}
                    </CardBody>

                        
                        <h3>Nombre, Medico Tratante</h3>


                    <Button onClick={e => this.setState({collapse: !this.state.collapse})}>Soy un boton</Button>
                    <Collapse isOpen={this.state.collapse}>
                        <Card>
                            <CardBody>
                                Hola mundo          
                        </CardBody>
                        </Card>
                    </Collapse>
                </Card>
            </Col>
            </Row>
          </div>
        );
    }
}

const mapStateToProps = state => ({
    paciente: state.paciente,
})

export default connect(mapStateToProps)(NotasDeIndicaciones);