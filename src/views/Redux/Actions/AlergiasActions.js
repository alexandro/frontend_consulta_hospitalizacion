export const UPDATE_ALERGIAS = 'alergiass:updateAlergias';

export function updateAlergias(newAlergias){
    return{
        type: UPDATE_ALERGIAS,
        payload: {
            alergias: newAlergias
        }
    }
}