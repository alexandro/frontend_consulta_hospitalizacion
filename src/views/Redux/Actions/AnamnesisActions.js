export const UPDATE_ANAMNESIS = 'anamnesiss:updateAnamnesis';

export function updateAnamnesis(newAnamnesis){
    return{
        type: UPDATE_ANAMNESIS,
        payload: {
            anamnesis: newAnamnesis
        }
    }
}