export const UPDATE_ANTECEDENTESC = 'antecedentescs:updateAntecedentesC';

export function updateAntecedentesC(newAntecedentesC){
    return{
        type: UPDATE_ANTECEDENTESC,
        payload: {
            antecedentesc: newAntecedentesC
        }
    }
}