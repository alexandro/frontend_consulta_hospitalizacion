export const UPDATE_ANTECEDENTESPNP = 'antecedentespnps:updateAntecedentesPNP';

export function updateAntecedentesPNP(newAntecedentesPNP){
    return{
        type: UPDATE_ANTECEDENTESPNP,
        payload: {
            antecedentespnp: newAntecedentesPNP
        }
    }
}