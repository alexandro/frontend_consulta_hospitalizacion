export const UPDATE_ANTECEDENTESPP = 'antecedentespps:updateAntecedentesPP';

export function updateAntecedentesPP(newAntecedentesPP){
    return{
        type: UPDATE_ANTECEDENTESPP,
        payload: {
            antecedentespp: newAntecedentesPP
        }
    }
}