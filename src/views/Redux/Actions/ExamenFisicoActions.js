export const UPDATE_EXAMENFISICO = 'examenfs:updateExamenFisico';

export function updateExamenFisico(newExamenFisico){
    return{
        type: UPDATE_EXAMENFISICO,
        payload: {
            examenf: newExamenFisico
        }
    }
}