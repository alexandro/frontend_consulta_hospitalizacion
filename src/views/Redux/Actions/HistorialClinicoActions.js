export const UPDATE_HISTORIALCLINICO = 'historialclinicos:updateHistorialClinico';

export function updateHistorialClinico(newHistorialClinico){
    return{
        type: UPDATE_HISTORIALCLINICO,
        payload: {
            historialclinico: newHistorialClinico
        }
    }
}