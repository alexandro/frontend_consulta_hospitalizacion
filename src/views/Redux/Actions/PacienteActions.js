export const UPDATE_PACIENTE = 'paciente:updatePaciente';

export function updatePaciente(newPaciente){
    return{
        type: UPDATE_PACIENTE,
        payload: {
            paciente: newPaciente
        }
    }
}