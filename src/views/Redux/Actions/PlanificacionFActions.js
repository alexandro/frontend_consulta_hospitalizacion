export const UPDATE_PLANIFICACIONF = 'planificacionfs:updatePlanificacionF';

export function updatePlanificacionF(newPlanificacionF){
    return{
        type: UPDATE_PLANIFICACIONF,
        payload: {
            planificacionf: newPlanificacionF
        }
    }
}