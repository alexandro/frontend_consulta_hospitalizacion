export const UPDATE_HC = 'hcs:updateHC';

export function updateHC(newHC){
    return{
        type: UPDATE_HC,
        payload: {
            hc: newHC
        }
    }
}