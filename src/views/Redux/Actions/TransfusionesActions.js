export const UPDATE_TRANSFUSIONES = 'transfusioness:updateTransfusiones';

export function updateTransfusiones(newTransfusiones){
    return{
        type: UPDATE_TRANSFUSIONES,
        payload: {
            transfusiones: newTransfusiones
        }
    }
}