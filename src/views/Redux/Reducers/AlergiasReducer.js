import { UPDATE_ALERGIAS } from '../Actions/AlergiasActions';

export default function alergiasReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_ALERGIAS:
            return payload.alergias;
        default:
            return state;
    }
}