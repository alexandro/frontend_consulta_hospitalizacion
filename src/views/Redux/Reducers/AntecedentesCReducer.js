import { UPDATE_ANTECEDENTESC } from '../Actions/AntecedentesCActions';

export default function antecedentescReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_ANTECEDENTESC:
            return payload.antecedentesc;
        default:
            return state;
    }
}