import { UPDATE_EXAMENFISICO } from '../Actions/ExamenFisicoActions';

export default function examenfisicoReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_EXAMENFISICO:
            return payload.examenf;
        default:
            return state;
    }
}