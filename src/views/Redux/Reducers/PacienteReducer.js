import { UPDATE_PACIENTE } from '../Actions/PacienteActions';

export default function pacienteReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_PACIENTE:
            return payload.paciente;
        default:
            return state;
    }
}