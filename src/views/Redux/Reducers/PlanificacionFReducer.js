import { UPDATE_PLANIFICACIONF } from '../Actions/PlanificacionFActions';

export default function planificacionfReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_PLANIFICACIONF:
            return payload.planificacionf;
        default:
            return state;
    }
}