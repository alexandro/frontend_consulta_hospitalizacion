import { UPDATE_SIGNOSVITALES } from '../Actions/SignosVitalesActions';

export default function signosvitalesReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_SIGNOSVITALES:
            return payload.signosvitales;
        default:
            return state;
    }
}