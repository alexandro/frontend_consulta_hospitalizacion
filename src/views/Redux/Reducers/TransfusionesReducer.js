import { UPDATE_TRANSFUSIONES} from '../Actions/TransfusionesActions';

export default function transfusionesReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_TRANSFUSIONES:
            return payload.transfusiones;
        default:
            return state;
    }
}